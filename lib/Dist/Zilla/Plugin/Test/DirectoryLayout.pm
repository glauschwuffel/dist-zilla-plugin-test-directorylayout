package Dist::Zilla::Plugin::Test::DirectoryLayout;

use strict;
use warnings;

# ABSTRACT: Test directory layout for standard compliance

use Moose;
use Test::DirectoryLayout;
use Test::More;

with 'Dist::Zilla::Role::TestRunner';

has add_dir => (is => 'ro', isa => 'ArrayRef[Str]', default => sub{[]});

=head1 METHODS

=method mvp_multivalue_args

Currently we have only one multi-value option: add_dir.

=cut

sub mvp_multivalue_args { return qw(add_dir) }

=method test

If additional directories are configured these are added to the list
of allowed directories.

Then we test the directory layout.

=cut

sub test {
  my ($self)=@_;
  _add_dirs ($self->add_dir) if @{$self->add_dir};

  directory_layout_ok;

  done_testing;
}

sub _add_dirs {
  my ($add_dir)=@_;

  my $allowed_dirs=get_allowed_dirs();
  push @$allowed_dirs, @$add_dir;
  set_allowed_dirs($allowed_dirs);
}

1;
